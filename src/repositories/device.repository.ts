import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor, HasManyRepositoryFactory} from '@loopback/repository';
import {InventoryDataSource} from '../datasources';
import {Device, DeviceRelations, Product, Log} from '../models';
import {ProductRepository} from './product.repository';
import {LogRepository} from './log.repository';

export class DeviceRepository extends DefaultCrudRepository<
  Device,
  typeof Device.prototype.id,
  DeviceRelations
> {

  public readonly product: BelongsToAccessor<Product, typeof Device.prototype.id>;

  public readonly logs: HasManyRepositoryFactory<Log, typeof Device.prototype.id>;

  constructor(
    @inject('datasources.inventory') dataSource: InventoryDataSource, @repository.getter('ProductRepository') protected productRepositoryGetter: Getter<ProductRepository>, @repository.getter('LogRepository') protected logRepositoryGetter: Getter<LogRepository>,
  ) {
    super(Device, dataSource);
    this.logs = this.createHasManyRepositoryFactoryFor('logs', logRepositoryGetter,);
    this.registerInclusionResolver('logs', this.logs.inclusionResolver);
    this.product = this.createBelongsToAccessorFor('product', productRepositoryGetter,);
    this.registerInclusionResolver('product', this.product.inclusionResolver);
  }
}
