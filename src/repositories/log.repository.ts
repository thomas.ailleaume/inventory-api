import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {InventoryDataSource} from '../datasources';
import {Log, LogRelations} from '../models';

export class LogRepository extends DefaultCrudRepository<
  Log,
  typeof Log.prototype.deviceId,
  LogRelations
> {
  constructor(
    @inject('datasources.inventory') dataSource: InventoryDataSource,
  ) {
    super(Log, dataSource);
  }
}
