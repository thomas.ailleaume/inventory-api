// Copyright IBM Corp. 2019. All Rights Reserved.
// Node module: @loopback/example-express-composition
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

export * from './product.controller';
export * from './device.controller';
export * from './device-product.controller';
export * from './product-device.controller';
export * from './device-log.controller';
