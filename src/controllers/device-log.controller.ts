import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Device,
  Log,
} from '../models';
import {DeviceRepository} from '../repositories';

export class DeviceLogController {
  constructor(
    @repository(DeviceRepository) protected deviceRepository: DeviceRepository,
  ) { }

  @get('/devices/{id}/logs', {
    responses: {
      '200': {
        description: 'Array of Device has many Log',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Log)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Log>,
  ): Promise<Log[]> {
    return this.deviceRepository.logs(id).find(filter);
  }

  @post('/devices/{id}/logs', {
    responses: {
      '200': {
        description: 'Device model instance',
        content: {'application/json': {schema: getModelSchemaRef(Log)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Device.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Log, {
            title: 'NewLogInDevice',
            exclude: ['id'],
            optional: ['deviceId']
          }),
        },
      },
    }) log: Omit<Log, 'id'>,
  ): Promise<Log> {
    return this.deviceRepository.logs(id).create(log);
  }

  @patch('/devices/{id}/logs', {
    responses: {
      '200': {
        description: 'Device.Log PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Log, {partial: true}),
        },
      },
    })
    log: Partial<Log>,
    @param.query.object('where', getWhereSchemaFor(Log)) where?: Where<Log>,
  ): Promise<Count> {
    return this.deviceRepository.logs(id).patch(log, where);
  }

  @del('/devices/{id}/logs', {
    responses: {
      '200': {
        description: 'Device.Log DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Log)) where?: Where<Log>,
  ): Promise<Count> {
    return this.deviceRepository.logs(id).delete(where);
  }
}
