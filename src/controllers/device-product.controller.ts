import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Device,
  Product,
} from '../models';
import {DeviceRepository} from '../repositories';

export class DeviceProductController {
  constructor(
    @repository(DeviceRepository)
    public deviceRepository: DeviceRepository,
  ) { }

  @get('/devices/{id}/product', {
    responses: {
      '200': {
        description: 'Product belonging to Device',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Product)},
          },
        },
      },
    },
  })
  async getProduct(
    @param.path.number('id') id: typeof Device.prototype.id,
  ): Promise<Product> {
    return this.deviceRepository.product(id);
  }
}
