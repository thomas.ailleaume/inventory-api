// Copyright IBM Corp. 2019,2020. All Rights Reserved.
// Node module: @loopback/example-express-composition
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {givenHttpServerConfig, Client, supertest} from '@loopback/testlab';
import {InventoryApplication} from '../../application';
import {ExpressServer} from '../../server';
import {Product} from '../../models/product.model';

export async function setupExpressApplication(): Promise<AppWithClient> {
  const server = new ExpressServer({rest: givenHttpServerConfig()});
  await server.boot();
  await server.start();

  const lbApp = server.lbApp;

  const client = supertest(server.app);

  return {server, client, lbApp};
}

export interface AppWithClient {
  server: ExpressServer;
  client: Client;
  lbApp: InventoryApplication;
}

/**
 * Generate a complete Note object for use with tests.
 * @param  A partial (or complete) Note object.
 */
export function givenNote(product?: Partial<Product>) {
  const data = Object.assign(
    {
      sku: 'KVE.VBE.000.001',
      description: 'KeyVibe version 3',
    },
    product,
  );
  return new Product(data);
}
