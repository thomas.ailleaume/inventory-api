import {Entity, model, property, belongsTo, hasMany} from '@loopback/repository';
import {Product, ProductWithRelations} from "./product.model";
import {Log} from './log.model';

@model({settings: {idInjection: false, mysql: {schema: 'inventory', table: 'Device'}}})
export class Device extends Entity {
  @property({
    type: 'date',
    required: true,
    mysql: {columnName: 'createdAt', dataType: 'datetime', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  createdAt: string;

  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'N'},
  })
  id: number;


  @property({
    type: 'string',
    required: true,
    length: 45,
    id: 3,
    mysql: {columnName: 'serialNumber', dataType: 'varchar', dataLength: 45, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  serialNumber: string;

  @property({
    type: 'string',
    required: true,
    length: 45,
    mysql: {columnName: 'state', dataType: 'varchar', dataLength: 45, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  state: string;

  @property({
    type: 'date',
    required: true,
    mysql: {columnName: 'updatedAt', dataType: 'datetime', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  updatedAt: string;

  @belongsTo(() => Product)
  productId: number;

  @hasMany(() => Log)
  logs: Log[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Device>) {
    super(data);
  }
}

export interface DeviceRelations {
  // describe navigational properties here
}

export type DeviceWithRelations = Device & DeviceRelations;
