import {Entity, model, property, hasMany} from '@loopback/repository';
import {Device} from './device.model';

@model({settings: {idInjection: false, mysql: {schema: 'inventory', table: 'Product'}}})
export class Product extends Entity {
  @property({
    type: 'date',
    required: true,
    mysql: {columnName: 'createdAt', dataType: 'datetime', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  createdAt: string;

  @property({
    type: 'string',
    required: true,
    length: 45,
    mysql: {columnName: 'denomination', dataType: 'varchar', dataLength: 45, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  denomination: string;

  @property({
    type: 'number',
    required: true,
    precision: 10,
    scale: 0,
    id: 1,
    mysql: {columnName: 'id', dataType: 'int', dataLength: null, dataPrecision: 10, dataScale: 0, nullable: 'N'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 45,
    mysql: {columnName: 'manufacturer', dataType: 'varchar', dataLength: 45, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  manufacturer: string;

  @property({
    type: 'string',
    required: true,
    length: 45,
    mysql: {columnName: 'type', dataType: 'varchar', dataLength: 45, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  type: string;

  @property({
    type: 'date',
    required: true,
    mysql: {columnName: 'updatedAt', dataType: 'datetime', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  updatedAt: string;

  @property({
    type: 'string',
    required: true,
    length: 45,
    mysql: {columnName: 'version', dataType: 'varchar', dataLength: 45, dataPrecision: null, dataScale: null, nullable: 'N'},
  })
  version: string;

  @hasMany(() => Device)
  devices: Device[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Product>) {
    super(data);
  }
}

export interface ProductRelations {
  // describe navigational properties here
}

export type ProductWithRelations = Product & ProductRelations;
